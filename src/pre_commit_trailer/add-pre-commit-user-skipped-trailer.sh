#!/usr/bin/env sh

# Remove any existing Pre-Commit-User-Skipped trailer since we add multiple occurrences of it.
#
# MacOS - doesn't understand --in-place or --regexp-extended so use the short form -i and -E
#       - raises an error when -i is specified with no extension
backup_suffix=''

if test "$(uname)" = "Darwin"; then
    backup_suffix=" ''"
fi

sed -i"$backup_suffix" "/Pre-Commit-User-Skipped:/d" "$1"

temp_file="$(mktemp)"

echo "$SKIP" > "$temp_file"

sed -i"$backup_suffix" "s/,/\n/g" "$temp_file"

skipped_hooks=$(sort "$temp_file" | uniq)

rm "$temp_file"

for hook in $skipped_hooks; do
    git interpret-trailers \
        --in-place \
        --if-exists add \
        --trim-empty \
        --trailer "Pre-Commit-User-Skipped: $hook" \
        "$1"
done
