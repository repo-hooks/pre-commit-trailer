#! /usr/bin/env sh

repository_root=$(git rev-parse --show-toplevel)

checksum=$(
    shasum --algorithm 256 \
        --UNIVERSAL \
        "$repository_root/.pre-commit-config.yaml"
)

# Remove any existing Pre-Commit-Config trailer.  This shouldn't be required but testing raised
# sufficient concerns to include it for additional assurance.
#
# MacOS - doesn't understand --in-place or --regexp-extended so use the short form -i and -E
#       - raises an error when -i is specified with no extension
backup_suffix=''

if test "$(uname)" = "Darwin"; then
    backup_suffix=" ''"
fi

sed -i"$backup_suffix" "/Pre-Commit-Config:/d" "$1"

git interpret-trailers \
    --in-place \
    --if-exists replace \
    --trim-empty \
    --trailer "Pre-Commit-Config: $checksum" \
    "$1"

sed -E -i"$backup_suffix" "s|(Pre-Commit-Config: .*) U$repository_root.*|\1|" "$1"
